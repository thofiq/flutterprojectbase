import 'package:base_structure_flutter/provider_setup.dart';
import 'package:base_structure_flutter/ui/router.dart';
import 'package:base_structure_flutter/utils/common/app_colors.dart';
import 'package:base_structure_flutter/utils/common/app_font.dart';
import 'package:base_structure_flutter/utils/common/app_route_paths.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();

}

class _MyAppState extends State<MyApp>  {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        title: 'Flutter Sample',
        theme: ThemeData(
          textTheme: GoogleFonts.ewertTextTheme(
            TextTheme(
              button: TextStyle(fontWeight: AppFont.fontWeightRegular),
              caption: TextStyle(fontWeight: AppFont.fontWeightRegular),
              body1: TextStyle(fontWeight: AppFont.fontWeightRegular),
              body2: TextStyle(fontWeight: AppFont.fontWeightRegular),
              subhead: TextStyle(fontWeight: AppFont.fontWeightRegular),
              title: TextStyle(fontWeight: AppFont.fontWeightRegular),
              headline: TextStyle(fontWeight: AppFont.fontWeightRegular),
              display1: TextStyle(fontWeight: AppFont.fontWeightRegular),
              display2: TextStyle(fontWeight: AppFont.fontWeightRegular),
            ),
          ),
          primaryColor: colorPrimary,
          accentColor: colorAccent,
          buttonColor: colorPrimary,
          buttonTheme: const ButtonThemeData(
              buttonColor: colorPrimary, textTheme: ButtonTextTheme.primary),
          primarySwatch: Colors.blue,
        ),
        initialRoute: RoutePaths.LOGIN,
        onGenerateRoute: Router.generateRoute,
        //home: LoginView(title: 'Flutter Demo Login Page'),
      ),
    );
  }
}


