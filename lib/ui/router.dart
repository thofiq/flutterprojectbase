import 'package:base_structure_flutter/ui/views/home_view.dart';
import 'package:base_structure_flutter/ui/views/login_view.dart';
import 'package:base_structure_flutter/utils/common/app_route_paths.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutePaths.HOME:
        return MaterialPageRoute(builder: (_) => HomeView());
      case RoutePaths.LOGIN:
        return MaterialPageRoute(builder: (_) => LoginView());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}
