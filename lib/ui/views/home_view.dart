import 'package:base_structure_flutter/core/serives/dependent_services/request_response/login/login_response.dart';
import 'package:base_structure_flutter/core/view_models/views/home_view_model.dart';
import 'package:base_structure_flutter/utils/common/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../base_widget.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  @override
  Widget build(BuildContext context) {
    String value = Provider.of<LoginResponse>(context).message;

    return BaseWidget<HomeViewModel>(
      viewModel: HomeViewModel(
        authRepository: Provider.of(context),
      ),
      onModelReady: (viewModel) => viewModel.setValueToLabel(value),
      builder: (context, loginViewModel, child) => Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(
            "Second Page",
            style: getAppBarTitleTextStyle(context),
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Reached 5',
              ),
              Text(
                '${loginViewModel.counter}',
                style: Theme.of(context).textTheme.display1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
