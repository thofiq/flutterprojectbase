import 'package:base_structure_flutter/core/serives/dependent_services/request_response/login/login_response.dart';
import 'package:base_structure_flutter/core/view_models/views/login_view_model.dart';
import 'package:base_structure_flutter/utils/common/app_route_paths.dart';
import 'package:base_structure_flutter/utils/common/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../base_widget.dart';

class LoginView extends StatefulWidget {
  LoginView({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    Provider.of<LoginResponse>(context);
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return BaseWidget<LoginViewModel>(
      viewModel: LoginViewModel(
        authRepository: Provider.of(context),
      ),
      child: Container(),
      builder: (context, loginViewModel, child) => Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(
            "First Page",
            style: getAppBarTitleTextStyle(context),
          ),
        ),
        body: Center(
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '\nIf equal to 5 means -> Next Page\n',
                ),
                Text(
                  '${loginViewModel.counter}',
                  style: Theme.of(context).textTheme.display1,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, RoutePaths.HOME)
                        .whenComplete(() {
                      loginViewModel.counter = 0;
                    });
                  },
                  child: Icon(Icons.chevron_right),
                ),
                // This trailing comma makes auto-formatting nicer for build methods.
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            loginViewModel.incrementCounter();

            if (loginViewModel.counter == 5) {
              if(Provider.of<LoginResponse>(context) != null) {
                Navigator.pushNamed(context, RoutePaths.HOME).whenComplete(() {
                  loginViewModel.counter = 0;
                });
              } else {
                 print("Detail Null");
              }
            }
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}
