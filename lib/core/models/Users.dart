class Users {
  String firstName;
  String lastName;
  String mobileNumber;
  String mailID;
  String id;
  String imageUrl;
  String storeId;
  String websSiteId;

  Users(
      {this.firstName,
      this.lastName,
      this.mobileNumber,
      this.mailID,
      this.id,
      this.storeId,
      this.websSiteId,
      this.imageUrl});

  @override
  String toString() {
    return 'ProfileInfo{firstName: $firstName, lastName: $lastName, mobileNumber: $mobileNumber, mailID:$mailID, id:$id, storeId:$storeId,webSiteId:$websSiteId,imageUrl:$imageUrl}';
  }
}
