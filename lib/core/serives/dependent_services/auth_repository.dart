import 'dart:async';
import 'dart:io';

import 'package:base_structure_flutter/core/serives/dependent_services/base_repository/base_repository.dart';
import 'package:base_structure_flutter/core/serives/dependent_services/request_response/login/login_response.dart';
import 'package:base_structure_flutter/core/serives/dependent_services/request_response/login/request.dart';
import 'package:base_structure_flutter/core/serives/independent_services/app_url.dart';
import 'package:base_structure_flutter/core/serives/independent_services/method.dart';
import 'package:base_structure_flutter/core/serives/independent_services/network_provider.dart';
import 'package:base_structure_flutter/utils/common/app_constants.dart';
import 'package:base_structure_flutter/utils/common/app_network_check.dart';

class AuthRepository extends BaseRepository {
  NetworkProvider _networkProvider;

  StreamController<LoginResponse> loginResponseController =
      StreamController<LoginResponse>.broadcast();

  Stream<LoginResponse> get loginResponse =>
      loginResponseController.stream.asBroadcastStream();
  String _countValue = "";

  StreamSubscription<LoginResponse> get streamSubscription => loginResponseController.stream.listen((va) {

  });

  AuthRepository({NetworkProvider networkProvider}) {
    _networkProvider = networkProvider;

    loginResponseController.stream.listen((data) {
      print("DataReceived: " + data.toString());
    }, onDone: () {
      print("Task Done1");
    }, onError: (error) {
      print("Some Error1 " + error.toString());
    });
  }



  globalSetUp(int value) {
    loginResponseController.add(LoginResponse(message: value.toString()));
  }

  /// just sample code
  //api: Login
  Future<bool> apiUserLogin(LoginRequest requestParams) async {
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      var fetchedLoginResponse = await _networkProvider.call(
          method: Method.POST,
          pathUrl: AppUrl.pathLogin,
          body: requestParams.toJson(),
          headers: headerContentTypeAndAccept);

      var hasResponse = fetchedLoginResponse != null;

      if (fetchedLoginResponse.statusCode == HttpStatus.ok) {
        if (hasResponse) {
          loginResponseController.onCancel();
          loginResponseController.add(fetchedLoginResponse);
        } else {
          loginResponseController
              .add(LoginResponse(message: 'Server error: Token is empty'));
        }
      } else if (fetchedLoginResponse.statusCode == HttpStatus.unauthorized) {
        loginResponseController
            .add(LoginResponse(message: 'Un Authorized User'));
      } else if (fetchedLoginResponse.statusCode == HttpStatus.notFound) {
        loginResponseController.add(LoginResponse(message: 'Data not found'));
      } else {
        //need to handel network connection error
        return null;
      }
      return hasResponse;
    } else {

      loginResponseController
          .add(LoginResponse(message: AppConstants.ERROR_INTERNET_CONNECTION));
      return false;
    }
  }
}
