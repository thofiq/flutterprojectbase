import 'dart:io';

class BaseRepository {

  final headerContentTypeAndAccept = {
    HttpHeaders.contentTypeHeader: "application/json",
    HttpHeaders.acceptHeader: "application/json"
  };
  final Map<String, String> mapAuthHeader = {
    HttpHeaders.authorizationHeader: 'Bearer nhl8gwc8vtee0bookm2vbkw31enngxhj'
  };

  Map<String, String> buildDefaultHeaderWithToken(String token) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  Map<String, String> buildOnlyHeaderWithToken(String token) {
    Map<String, String> header = {};
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  String getFormattedToken(String token) {
    return 'Bearer $token';
  }
}
