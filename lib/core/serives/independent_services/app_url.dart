class AppUrl {

  static final baseHost = "colan.made.code";
  static final baseHttp = "https://";
  static final baseUrl = "$baseHttp$baseHost";
  static final baseImageUrl1 = "$baseUrl/pub/media/catalog/category";

  //register
  static final pathRegister = "/rest/default/V1/customers"; //POST
  //login
  static final pathLogin = "/rest/default/V1/integration/customer/token"; //POST
}