import 'package:base_structure_flutter/core/serives/dependent_services/auth_repository.dart';
import 'package:base_structure_flutter/core/serives/dependent_services/request_response/login/request.dart';
import 'package:flutter/material.dart';

import '../base_view_model.dart';

class LoginViewModel extends BaseViewModel {
  AuthRepository _authRepository;

  int _counter = 0;

  LoginViewModel({@required AuthRepository authRepository}) : _authRepository = authRepository;



  int get counter => _counter;

  set counter(int value) {
    _counter = value;
    notifyListeners();
  }

  Future<bool> login(String emailId, String password) async {
    super.setBusy(true);
    //call api here
    var success = _authRepository.apiUserLogin(LoginRequest(username: emailId, password: password));

    super.setBusy(false);
    return success;
  }

  void incrementCounter() {
      counter++;
      _authRepository.globalSetUp(counter);
  }

}