import 'package:base_structure_flutter/core/serives/independent_services/network_provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:provider/provider.dart';

import 'core/serives/dependent_services/auth_repository.dart';
import 'core/serives/dependent_services/request_response/login/login_response.dart';

List<SingleChildWidget> providers = [
  ...independentServices,
  ...dependentServices,
  ...uiConsumableProviders,
];

List<SingleChildWidget> independentServices = [
  Provider<NetworkProvider>(create: (_) => NetworkProvider())
];
List<SingleChildWidget> dependentServices = [
  ProxyProvider<NetworkProvider, AuthRepository>(
      update: (context, networkProvider, authRepository) =>
          AuthRepository(networkProvider: networkProvider))
];
List<SingleChildWidget> uiConsumableProviders = [
  StreamProvider<LoginResponse>(
      initialData: LoginResponse(message: "1", token: "", trace: ""),
      create: (context) =>
          Provider.of<AuthRepository>(context, listen: false).loginResponse)
];
